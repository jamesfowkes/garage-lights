#include <stdint.h>
#include <string.h>

#include <Wire.h>

#include "raat.hpp"

#include "raat-oneshot-timer.hpp"
#include "raat-oneshot-task.hpp"
#include "raat-task.hpp"

static const raat_devices_struct * s_pDevices = NULL;
static const raat_params_struct * s_pParams = NULL;

static float s_voltage = 0.0f;

static bool s_voltage_ok = false;

static void display_enable(bool en)
{
    raat_logln(LOG_APP, "DSP%d", en ? 1 : 0);
    //  Display setup, turn off by default
    uint8_t display_ctrl_data[] = {0x04, en ? 0x01 : 0x00};

    Wire.beginTransmission(0x10);
    Wire.write(display_ctrl_data, 3);
    Wire.endTransmission();
}

static float get_voltage(AnalogInput& input)
{
    unsigned int raw_adc = input.reading();
    float voltage = (raw_adc * 5.0f) / 1023;
    return voltage * CONVERSION_FACTOR;
}

void voltage_task_fn(RAATTask& this_task, void * pData)
{
    (void)this_task; (void)pData;
    s_voltage = (s_voltage + get_voltage(*(s_pDevices->pBatteryVoltageInput)))/2;
    s_voltage_ok = (uint16_t)(s_voltage*1000) >= s_pParams->pLowVoltagemv->get();

    int whole = (int)s_voltage;
    int decimal = (int)((s_voltage - whole) * 100);

    raat_logln(LOG_APP, "%d.%02d", (int)whole, (int)decimal);
}
static RAATTask s_voltage_task(5000, voltage_task_fn, NULL);

void display_task_fn(RAATTask& this_task, void * pData)
{
    (void)this_task, (void)pData;
    uint16_t u16_voltage = (uint16_t)(s_voltage*10);
    uint8_t data[] = {0x01, 0xFF, 0xFF, 10};
    memcpy(&data[1], &u16_voltage, sizeof(u16_voltage));
    Wire.beginTransmission(0x10);
    Wire.write(data, 4);
    Wire.endTransmission();
}
static RAATTask s_display_task(1000, display_task_fn, NULL);

void light_control_task_fn(RAATTask& this_task, void * pData)
{
    (void)this_task; (void)pData;
    static bool light_state = false;
    bool new_state = false;
    raat_devices_struct * pDevices = (raat_devices_struct *)pData;

    bool ldr_triggered = pDevices->pLDRTrigger->state();
    bool override = !pDevices->pOverrideSwitch->state();
    bool door_open = pDevices->pDoorDetect->state();

    if (light_state)
    {
        // Only turn off based on switches, not the LDR
        new_state = s_voltage_ok && (door_open || override);
    }
    else
    {
        new_state = s_voltage_ok && ((door_open && ldr_triggered) || override);
    }

    if (!light_state && new_state)
    {
        raat_logln_P(LOG_APP, PSTR("Lights on (V:%d, D:%d, L:%d, O:%d)"), s_voltage_ok, door_open, ldr_triggered, override);
        display_enable(true);
    }
    else if (light_state && !new_state)
    {
        raat_logln_P(LOG_APP, PSTR("Lights off (V:%d, D:%d, L:%d, O:%d)"), s_voltage_ok, door_open, ldr_triggered, override);
        display_enable(false);
    }

    light_state = new_state;
    pDevices->pLights->set(light_state);

}
static RAATTask s_light_control_task(200, light_control_task_fn, NULL);

void raat_custom_setup(const raat_devices_struct& devices, const raat_params_struct& params)
{
    s_pDevices = &devices;
    s_pParams = &params;
    s_pParams->pLowVoltagemv->set(10000);

    raat_logln_P(LOG_APP, PSTR("Garage Lights: ready"));
    delay(10);
    raat_logln_P(LOG_APP, PSTR("Low Voltage: %dmV"), s_pParams->pLowVoltagemv->get());
    s_voltage = get_voltage(*(devices.pBatteryVoltageInput));

    Wire.begin();

    delay(10);
    display_enable(false);
}

void raat_custom_loop(const raat_devices_struct& devices, const raat_params_struct& params)
{
    (void)params;

    s_voltage_task.run((void*)&devices);
    s_display_task.run((void*)&devices);
    s_light_control_task.run((void*)&devices);
}
